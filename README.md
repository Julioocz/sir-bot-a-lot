# Sir Bot a Lot

[![build status](https://gitlab.com/PythonDevCommunity/sir-bot-a-lot/badges/master/build.svg)](https://gitlab.com/PythonDevCommunity/sir-bot-a-lot/commits/master)
[![coverage report](https://gitlab.com/PythonDevCommunity/sir-bot-a-lot/badges/master/coverage.svg)](https://gitlab.com/PythonDevCommunity/sir-bot-a-lot/commits/master)

A slack bot built for the people and by the people of the python developers slack community. https://pythondev.slack.com/
Want to contribute?
Get an invite!
http://pythondevelopers.herokuapp.com/

* Documentation: https://sir-bot-a-lot.readthedocs.io
